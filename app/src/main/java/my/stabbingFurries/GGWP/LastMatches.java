package my.stabbingFurries.GGWP;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class LastMatches extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_matches);

        Bundle b = getIntent().getExtras();
        String username = b.getString("username");

        TextView txtLastGamesof = (TextView) findViewById(R.id.txtLastMatchesLastGames);
        txtLastGamesof.setText(MainActivity.res.getString(R.string.lastGamesOf)+" "+username);

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ninfo = cm.getActiveNetworkInfo();

        if(ninfo != null){
            if(ninfo.isConnected()){
                try{
                    System.out.println("--------------------------------------------A BUSCAR");
                    new JsonTask(this).execute(new URL(MainActivity.SERVERHOSTNAME+"/getPartidas.php?name="+username));
                    System.out.println("--------------------------------------------BUSCAO");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    HttpURLConnection con;
    public Partidas partidasObtenidas = null;

    public class JsonTask extends AsyncTask<URL,Void, Partidas> {

        private ProgressDialog pd;
        private Activity act;

        public JsonTask(Activity activity){
            act =activity;
            pd = new ProgressDialog(activity);
        }

        @Override
        protected void onPreExecute(){
            pd.setMessage("Searching player's games...");
            pd.setIndeterminate(false);
            pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pd.setCancelable(false);
            pd.show();
        }

        @Override
        protected Partidas doInBackground(URL... urls) {
            Partidas partidas = null;
            try {
                con = (HttpURLConnection) urls[0].openConnection();
                con.setConnectTimeout(10000);
                con.setReadTimeout(10000);

                int state = con.getResponseCode();
                if(state == 200){
                    System.out.println("-------------------------------------------- pillar el input");

                    InputStream in = new BufferedInputStream(con.getInputStream());
                    partidas = GsonPartidaParser.read(in);
                    System.out.println("--------------------------------------------tengo user");

                }
            }catch (Exception e){
                System.err.println("Unpareseable, partidas inexistentes");
            }finally {
                con.disconnect();
            }
            return partidas;
        }

        @Override
        protected void onPostExecute(Partidas partidas) {
            System.out.println("--------------------------------------------ACABADO");

            if(pd.isShowing())
                pd.dismiss();

            if(partidas != null){
                partidasObtenidas = partidas;
                System.out.println(partidas);
                CustomAA c = new CustomAA(act);
                ListView listView = (ListView) findViewById(R.id.listLastMatches);
                listView.setAdapter(c);


            }else{
                Toast.makeText(getApplicationContext(),"El usuario no tiene partidas",Toast.LENGTH_LONG).show();
            }
        }
    }

    class CustomAA extends ArrayAdapter {

        Activity context;

        public CustomAA(Activity context){
            super(context, R.layout.partidalayout, partidasObtenidas.getPartidas());
            this.context = (Activity) context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            Partida p = partidasObtenidas.getPartidas().get(position);
            System.out.println(p);

            LayoutInflater inflater = context.getLayoutInflater();
            View item = inflater.inflate(R.layout.partidalayout, null);

            TextView idPartida = (TextView) item.findViewById(R.id.txtCTXPartidasIdPartida);
            idPartida.setText(p.getIdPartida()+"");

            TextView personaje = (TextView) item.findViewById(R.id.txtCTXPartidasPersonaje);
            personaje.setText(p.getPersonaje());

            TextView k = (TextView) item.findViewById(R.id.txtCTXPartidasKills);
            k.setText(p.getKills()+"");

            TextView d = (TextView) item.findViewById(R.id.txtCTXPartidasDeaths);
            d.setText(p.getDeaths()+"");

            TextView a = (TextView) item.findViewById(R.id.txtCTXPartidasAssists);
            a.setText(p.getAssists()+"");

            TextView creeps = (TextView) item.findViewById(R.id.txtCTXPartidasCreeps);
            creeps.setText(p.getCreeps()+"");

            TextView winLose = (TextView) item.findViewById(R.id.txtCTXPartidasWin);
            if(p.getTeam().equals(p.getWinTeam())){
                winLose.setTextColor(Color.GREEN);
                winLose.setText("Win");
            }else{
                winLose.setTextColor(Color.RED);
                winLose.setText("Lose");
            }

            TextView startTime = (TextView) item.findViewById(R.id.txtCTXPartidasStartTime);
            startTime.setText(p.getStartTime());

            TextView endTime = (TextView) item.findViewById(R.id.txtCTXPartidasEndTime);
            endTime.setText(p.getEndTime());


            return (item);
        }


    }
}