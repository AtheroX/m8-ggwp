package my.stabbingFurries.GGWP;

import java.util.List;

public class Partidas {

    private List<Partida> partidas;

    public List<Partida> getPartidas(){
        return partidas;
    }

    public void setPartidas(List<Partida> partidas) {
        this.partidas = partidas;
    }

    @Override
    public String toString() {
        return "Partidas{ " + partidas +
                '}';
    }
}
