package my.stabbingFurries.GGWP;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;

public class SearchPlayer extends AppCompatActivity {
    SoundManager soundManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_player);

        Bundle b = getIntent().getExtras();
        User u = new User(b.getString("username"),b.getInt("level"),b.getInt("exp"),b.getString("icon"),b.getLong("date"));
        System.out.println("----------------------------------------------------------------------------"+u);

        TextView username = (TextView) findViewById(R.id.lblSearchPlayerUsername);
        username.setText(u.getUsername());

        soundManager = new SoundManager(this);
        soundManager.setVol(200);

        Button reload = (Button) findViewById(R.id.btnSearchPlayerReload);
        reload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundManager.play(SoundLibrary.boton2);
                Intent i = new Intent();
                i.putExtra("username", u.getUsername());
                System.out.println("out "+i.getExtras().toString());
                setResult(RESULT_OK, i);
                finish();
            }
        });

        Button matches = (Button) findViewById(R.id.btnSearchPlayerLastMatches);
        matches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                soundManager.play(SoundLibrary.boton1);
                Intent i = new Intent(getApplicationContext(), LastMatches.class);
                i.putExtra("username", u.getUsername());
                startActivityForResult(i, 1);
            }
        });

        TextView level = (TextView) findViewById(R.id.txtSearchPlayerNivel);
        level.setText(u.getLevel()+"");

        TextView exp = (TextView) findViewById(R.id.txtSearchPlayerExp);
        exp.setText(u.getExp()+"");

        TextView lastUpd = (TextView) findViewById(R.id.txtSearchPlayerLastUpdate);
        lastUpd.setText(MainActivity.res.getText(R.string.lastUpdate)+" "+u.getDate().toString());



        final ImageView img = (ImageView) findViewById(R.id.imgSearchPlayerIcon);
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ninfo = cm.getActiveNetworkInfo();

        if (ninfo != null) {
            if (ninfo.isConnected()) {
                try {
                    System.out.println("--------------------------------------------A BUSCAR");
                    new ImgRequest(img).execute(new URL(MainActivity.SERVERHOSTNAME + "/img/icons/"+u.getIcon()));
                } catch (Exception e) {

                }
            }
        }


    }

    private class ImgRequest extends AsyncTask<URL, Void, Bitmap>{

        ImageView img;

        public ImgRequest(ImageView img){
            this.img = img;
        }


        @Override
        protected Bitmap doInBackground(URL... urls) {
            try {
                HttpURLConnection connection = (HttpURLConnection) urls[0].openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                return myBitmap;

            }catch (Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            img.setImageBitmap(bitmap);
        }
    }

}