package my.stabbingFurries.GGWP;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class GsonUserParser {

    public static User read(InputStream in) throws IOException{
        Gson gson = new Gson();

        JsonReader reader = new JsonReader(new InputStreamReader(in,"UTF-8"));

        User u = gson.fromJson(reader,User.class);
        u.setDate();
        System.out.println(u);
        reader.close();
        System.out.println("--------------------------------------------devolver");

        return u;
    }
}
